//
//  ResponseCode.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

enum ResponseCode: Int{
    case SUCCESS = 1
    case FAIL = 2
}
