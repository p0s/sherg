//
//  SocketServer.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import CocoaAsyncSocket
import ObjectMapper

class SocketServer {
    
    var serverSocket: GCDAsyncSocket?
    var clientSocket: GCDAsyncSocket?
    //var clientSocketList: [GCDAsyncSocket] = []
    var jsonResHandlerFactory: JsonResHandlerFactory?
    var port: Int?
    
    init(port: Int) {
        
        jsonResHandlerFactory = JsonResHandlerFactory()
        self.port = port
        listen()
    
    }
    
    func listen() {
        
        serverSocket = GCDAsyncSocket(delegate: self, delegateQueue: dispatch_get_main_queue())
        
        do {
            try serverSocket?.acceptOnPort(UInt16(port!))
            //Listen success
        }catch _ {
            //Listen fail
        }
    }
}

extension SocketServer: GCDAsyncSocketDelegate {
    
    //New Socket Connected
    func socket(sock: GCDAsyncSocket!, didAcceptNewSocket newSocket: GCDAsyncSocket!) {
        
        clientSocket = newSocket
        
        //First time read data
        clientSocket!.readDataWithTimeout(-1, tag: 0)
        
        //Add socket to list
        //clientSocketList.append(clientSocket!)
    }
    
    func socket(sock: GCDAsyncSocket!, didReadData data: NSData!, withTag tag: Int) {
        
        let commBase = Mapper<CommBase>().map(AESHelper.decrypt(data))
        
        let jsonResHandler = jsonResHandlerFactory?.create(commBase!, clientSocket: sock)
        
        if jsonResHandler != nil {
            jsonResHandler?.handleJsonData(commBase!)
        } else {
            // error jsonResHandlerFactory unable to find the handler
        }
        
        //read data again
        sock.readDataWithTimeout(-1, tag: 0)
    }
    
}
