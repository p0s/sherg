//
//  OrderViewController.swift
//  PosSocket
//
//  Created by SHERG on 27/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var names = ["RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET","RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET", "RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    /*override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let ccell = collectionView.dequeueReusableCellWithReuseIdentifier("ccell", forIndexPath: indexPath) as UICollectionViewCell
        
        return ccell
    }*/
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
        
        cell.name.text = names [indexPath.row]
        
        return cell
    }

}
