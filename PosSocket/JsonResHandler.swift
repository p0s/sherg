//
//  JsonResHandler.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import CocoaAsyncSocket
import ObjectMapper

class JsonResHandler {
    
    var clientSocket: GCDAsyncSocket?
    var commandCode: CommandCode
    var requestCode: RequestCode
    
    
    init(requestCode: RequestCode, commandCode: CommandCode, clientSocket: GCDAsyncSocket) {
        self.requestCode = requestCode
        self.commandCode = commandCode
        self.clientSocket = clientSocket
    }
    
    func handleJsonData(commBase: CommBase) {
        
    }
    
    func responseDataToClient(data: Mappable, responseCode: ResponseCode, messageCode: String) {
        
        JSONHelper.ResponseDataToClient(clientSocket, data: data, cmdCode: commandCode, reqCode: requestCode, resCode: responseCode, msgCode: messageCode)
        
    }
}
