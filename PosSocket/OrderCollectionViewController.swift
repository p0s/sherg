//
//  OrderCollectionViewController.swift
//  PosSocket
//
//  Created by SHERG on 28/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import UIKit

class OrderCollectionViewController: UICollectionViewController {
    
    var Array = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Array = ["RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET","RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET", "RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Array.count
    }
     
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
        
        var name = cell.viewWithTag(1) as! UILabel
        name.text = Array[indexPath.row]
        
        
        return cell
    }
    
}

