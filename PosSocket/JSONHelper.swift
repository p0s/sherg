//
//  JSONHelper.swift
//  PosSocket
//
//  Created by SHERG on 19/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import CocoaAsyncSocket
import ObjectMapper

class JSONHelper {
    
    static func ResponseDataToClient(clientSocket: GCDAsyncSocket?, data: Mappable, cmdCode: CommandCode, reqCode: RequestCode, resCode: ResponseCode, msgCode: String) {
        
         let commBase = CommBase(cmdCode: cmdCode, msgCode: msgCode, reqCode: reqCode, resCode: resCode, data: data)
    
        //write data to client, timeout setting -1 will not overtime
        clientSocket?.writeData(AESHelper.encrypt(Mapper().toJSONString(commBase)!), withTimeout: -1, tag: 0)
    }
}
