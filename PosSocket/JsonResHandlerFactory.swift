//
//  JsonResHandlerFactory.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import CocoaAsyncSocket

class JsonResHandlerFactory {
    
    func create(commBase: CommBase, clientSocket: GCDAsyncSocket!) -> JsonResHandler? {
    
        switch commBase.ReqCode!.rawValue {
            
            case RequestCode.COMMON.rawValue  :
                return CommonHandler(requestCode: commBase.ReqCode!, commandCode: commBase.CmdCode!, clientSocket: clientSocket)
            case RequestCode.MEMBERSHIP.rawValue  :
                return nil
            default  :
                return nil

        }
    }
}
