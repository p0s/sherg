//
//  RequestCode.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

enum RequestCode: Int{
    case COMMON = 1
    case MEMBERSHIP = 2
}