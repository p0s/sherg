//
//  CommonHandler.swift
//  PosSocket
//
//  Created by SHERG on 19/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

class CommonHandler: JsonResHandler {
    
    override func handleJsonData(commBase: CommBase) {
        
        switch commBase.CmdCode!.rawValue {
            
            case CommandCode.PING.rawValue:
                ping()
            default:
                break
        }
    }
    
    func ping() {
        
    }

}
