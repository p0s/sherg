//
//  CommandCode.swift
//  PosSocket
//
//  Created by SHERG on 16/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

enum CommandCode: Int{
    case LOGIN = 1
    case LOGOUT = 2
    case PING = 3
    
}