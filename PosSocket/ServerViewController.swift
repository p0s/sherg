//
//  ServerViewController.swift
//  PosSocket
//
//  Created by SHERG on 09/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import UIKit
import CocoaAsyncSocket
import ObjectMapper

class ServerViewController: UIViewController {
    
    //端口
    @IBOutlet weak var portTF: UITextField!
    //消息
    @IBOutlet weak var msgTF: UITextField!
    //信息显示
    @IBOutlet weak var infoTV: UITextView!
    
    //服务端和客户端的socket引用
    var serverSocket: GCDAsyncSocket?
    var clientSocket: GCDAsyncSocket?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //对InfoTextView添加提示内容
    func addText(text: String) {
        infoTV.text = infoTV.text.stringByAppendingFormat("%@\n", text)
    }
    
    @IBAction func listeningAct(sender: AnyObject) {
        
        serverSocket = GCDAsyncSocket(delegate: self, delegateQueue: dispatch_get_main_queue())
        
        do {
            try serverSocket?.acceptOnPort(UInt16(portTF.text!)!)
            addText("监听成功")
        }catch _ {
            addText("监听失败")
        }
        
    }
    
    @IBAction func sendAct(sender: AnyObject) {
        
        let loginModel = LoginModel(id: "92123555", password: "abc123")
        
        //let data = msgTF.text?.dataUsingEncoding(NSUTF8StringEncoding)
        //向客户端写入信息   Timeout设置为 -1 则不会超时, tag作为两边一样的标示
        clientSocket?.writeData(AESHelper.encrypt(Mapper().toJSONString(loginModel)!), withTimeout: -1, tag: 0)
        
    }
    
}

extension ServerViewController: GCDAsyncSocketDelegate {
    
    //当接收到新的Socket连接时执行
    func socket(sock: GCDAsyncSocket!, didAcceptNewSocket newSocket: GCDAsyncSocket!) {
        
        addText("连接成功")
        addText("连接地址" + newSocket.connectedHost!)
        addText("端口号" + String(newSocket.connectedPort))
        clientSocket = newSocket
        
        //第一次开始读取Data
        clientSocket!.readDataWithTimeout(-1, tag: 0)
    }
    
    func socket(sock: GCDAsyncSocket!, didReadData data: NSData!, withTag tag: Int) {
        
        
        addText(AESHelper.decrypt(data))
        
        if let loginModel = Mapper<LoginModel>().map(AESHelper.decrypt(data)){
            addText(loginModel.id!)
            addText(loginModel.password!)
        }
        
        //let message = String(data: data,encoding: NSUTF8StringEncoding)
        //addText(message!)
        //再次准备读取Data,以此来循环读取Data
        sock.readDataWithTimeout(-1, tag: 0)
    }
    
}