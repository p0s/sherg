//
//  AESHelper.swift
//  PosSocket
//
//  Created by SHERG on 16/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//
import CryptoSwift

class AESHelper {
    
    static let key = "A2A3A4A5A7A8A9AAACADAEAFB1B2B3B4"
    static let iv = "000102030405060708090a0b0c0d0e0f"
    
    static func encrypt (stringToEncrypt: String) -> NSData {

        let byteArray = Array(stringToEncrypt.utf8)
        
        let nsDataKey = key.dataFromHexadecimalString()
        let nsDataIv = iv.dataFromHexadecimalString()
        
        var byteKey = [UInt8](count: nsDataKey!.length, repeatedValue: 0)
        nsDataKey!.getBytes(&byteKey, length: nsDataKey!.length)
        
        var byteIv = [UInt8](count: nsDataIv!.length, repeatedValue: 0)
        nsDataIv!.getBytes(&byteIv, length: nsDataIv!.length)
        
        let encryptedBytes = try! AES(key: byteKey, iv: byteIv, blockMode: .CBC, padding: PKCS7()).encrypt(byteArray)

        let nsData = NSData(bytes: encryptedBytes, length: encryptedBytes.count)
        
        return nsData.base64EncodedDataWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
    }
    
    static func decrypt (message: NSData) -> String {
        
        let decodeMsg = NSData(base64EncodedData: message, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        var byteArray = [UInt8](count: decodeMsg.length, repeatedValue: 0)
        decodeMsg.getBytes(&byteArray, length: decodeMsg.length)
        
        let nsDataKey = key.dataFromHexadecimalString()
        let nsDataIv = iv.dataFromHexadecimalString()
        
        var byteKey = [UInt8](count: nsDataKey!.length, repeatedValue: 0)
        nsDataKey!.getBytes(&byteKey, length: nsDataKey!.length)
        
        var byteIv = [UInt8](count: nsDataIv!.length, repeatedValue: 0)
        nsDataIv!.getBytes(&byteIv, length: nsDataIv!.length)
        
        let decryptedBytes: [UInt8] = try! AES(key: byteKey, iv: byteIv, blockMode: .CBC, padding: PKCS7()).decrypt(byteArray)
        
        let unencryptedString = NSString(bytes: decryptedBytes, length: decryptedBytes.count, encoding: NSUTF8StringEncoding) as! String
        
        return unencryptedString
    }

}

extension String {
    /// http://stackoverflow.com/questions/26501276/converting-hex-string-to-nsdata-in-swift
    ///
    /// Create NSData from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a NSData object. Note, if the string has any spaces, those are removed. Also if the string started with a '<' or ended with a '>', those are removed, too. This does no validation of the string to ensure it's a valid hexadecimal string
    ///
    /// The use of `strtoul` inspired by Martin R at http://stackoverflow.com/a/26284562/1271826
    ///
    /// - returns: NSData represented by this hexadecimal string. Returns nil if string contains characters outside the 0-9 and a-f range.
    func dataFromHexadecimalString() -> NSData? {
        let trimmedString = self.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<> ")).stringByReplacingOccurrencesOfString(" ", withString: "")
        // make sure the cleaned up string consists solely of hex digits, and that we have even number of them
        let regex = try! NSRegularExpression(pattern: "^[0-9a-f]*$", options: .CaseInsensitive)
        let found = regex.firstMatchInString(trimmedString, options: [], range: NSMakeRange(0, trimmedString.characters.count))
        if found == nil || found?.range.location == NSNotFound || trimmedString.characters.count % 2 != 0 {
            return nil
        }
        // everything ok, so now let's build NSData
        let data = NSMutableData(capacity: trimmedString.characters.count / 2)
        for var index = trimmedString.startIndex; index < trimmedString.endIndex; index = index.successor().successor() {
            let byteString = trimmedString.substringWithRange(Range<String.Index>(start: index, end: index.successor().successor()))
            let num = UInt8(byteString.withCString { strtoul($0, nil, 16) })
            data?.appendBytes([num] as [UInt8], length: 1)
        }
        return data
    }
}