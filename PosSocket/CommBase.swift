//
//  CommBase.swift
//  PosSocket
//
//  Created by SHERG on 18/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import ObjectMapper

class CommBase: Mappable{
    
    var CmdCode: CommandCode?
    var MsgCode: String?
    var ReqCode: RequestCode?
    var ResCode: ResponseCode?
    var Data: Mappable?
    
    init(cmdCode: CommandCode, msgCode: String, reqCode: RequestCode, resCode: ResponseCode, data: Mappable) {
        self.CmdCode = cmdCode
        self.MsgCode = msgCode
        self.ReqCode = reqCode
        self.ResCode = resCode
        self.Data = data
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        CmdCode <- map["CmdCode"]
        MsgCode <- map["MsgCode"]
        ReqCode  <- map["ReqCode"]
        ResCode <- map["ResCode"]
        Data  <- map["Data"]
    }
    
}
