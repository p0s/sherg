//
//  TableVC.swift
//  PosSocket
//
//  Created by SHERG on 28/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import UIKit

class TableVC: UITableViewController {
    
    var Array = [String]()
    
    override func viewDidLoad() {
        //super.viewDidLoad()
        
        Array = ["RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET","RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET", "RICE", "STEAK", "WATER", "NOODLES", "WATER", "CAKE", "LUNCH SET", "DINNER SET"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        
        cell.textLabel?.text = Array[indexPath.row]
        
        return cell
    }
}
