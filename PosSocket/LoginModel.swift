//
//  LoginModel.swift
//  PosSocket
//
//  Created by SHERG on 11/09/2016.
//  Copyright © 2016 SHERG. All rights reserved.
//

import ObjectMapper

class LoginModel: Mappable {
    
    var id: String?
    var password: String?
    
    init(id: String, password: String) {
        self.id = id
        self.password = password
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        password  <- map["password"]
    }
    
}
